# Dejiny designu - Zimný semester

## Osnova
1. Vymezení předmětu "dějin designu", jeho rozdílné interpretace. Grafický a průmyslový design
2. "Proto-design": historické formování některých aspektů designu v předprůmyslové éře
3. První průmyslová revoluce, krize koncepce užitého umění a zrod profese designera
4. Vynález litografie a zrod plakátu
5. Reformní hnutí 2. poloviny 19. století
6. Formování secese a její zenit v období přelomu 19. a 20. století
7. Alfons Mucha a secese v Čechách
8. Peter Behrens a zrod Corporate Identity
9. Raná moderna

## Profesia dizajnéra
- **oddelenie procesu navrhovania od procesu materiálnej realizácie**

## Historické formovanie aspektov dizajnu
 - profesia designéra sa zrodila v obodbí priemyselnej revolúcie (~1780)
 - existujú názory že dizajnér vznikol už vtedy, keď človek začal používať prvé pracovné nástroje - v nástrojoch možno vidieť premýšlanie nad formou
 - v staroveku architekti, staviteľia lodí či vozov - využívali kresbu ako prostriedok (da Vinci, di Giorgio)

## Johann Gutenberg (~1400 - 1468)
 - kovorytec, vynálezca
 - vynálezy a inovácie v oblasti mechanickej knihtlače

## Albrecht Durer (1471 - 1528)
 - námety s prírodou
 - známe dielo "Apocalypsa"
 - podpis ako monogram

![Monogram](/images/durer-monogram.jpg)

## Leonardo da Vinci (1452 - 1519)

![Gigantická kuša](/images/davinci-crossbow.jpg)
![Bojové vozidlo](/images/davinci-tank.jpg)

 - občas popisované ako prototyp moderných tankov
 - inšpirované štítom korytnačky
 - v r. 2010 skupina inžiniérov vytvorila model podľa originálnych podkladov

## Francesco di Giorgio (1439 - 1501)

![Machines](/images/giorgio-machines.jpg)
![Machines](/images/giorgio-machines-2.jpg)

## James Watt
 - zdokonalil parný stroj tak, aby sa dal priemyselne využiť (~1780)

## Josiah Wedgwood (1730 - 1795)

![Portlandská váza](/images/wedgwood-portland-vase.jpg)

 - skúsený hrnčiar
 - problémy s kolenom kvôli ovčím kiahňom - koncentroval sa na návrh keramiky, sám ju neprodukoval

## Vynález litografie  (~1790)
 - Alois Senefelder
 - 1796 - prvý litografický tisk - notový záznam Franza Gleißnera
 - zdokonaloval techniku - zostrojil tlačiarenský lis (1789)
 - prvé album litografií - polyautografia - 1802 Mníchov, 1803 Londýne a 1804 v Mníchove

## Jules Chéret
 - otec moderného plagátu

## Henri de Tolouse Lautrec (1864 - 1901)
 - litografické plagáty

![Tournee du Chat Noir](/images/lautrec-black-cat.jpg)
![Moulin Rouge](/images/lautrec-moulin-rouge.jpg)

## Augustus W. N. Pugin

![Gotický stôl](/images/pugin-gothic-desk.jpg)
![Svietnik](/images/pugin-candelabrum.jpg)

## School of Design (Royal collage of Art) (1837)
 - priame uplatnenie umenia vo výrobe
 - kresliarska škola, bez vplyvu na štandard priemyseľnej produkcie
 - študenti napr.: Christopher Dresser, Gottfried Semper

![Odbor sochy](/images/rca.jpg)

## Svetová výstava v Londýne (Great exibition) (1. 5. - 15. 10. 1851)

![Kryštálový palác](/images/Crystal_Palace.png)
![Kryštálový palác](/images/crystal_palace_2.jpg)

 - odohrávala sa v kryštálovom paláci
 - výstava priemyslu a kultúry jednotlivých krajin
 - iniciátor - princ Albert - prezident Royal Society, spolupracoval s Henry Colom, Fracisom Henrym a Sirom Charlesom Dilkom
 - architekt - Joseph Paxton - liatinová kostra vyplnená sklom
 - exponáty z celej Veľkej Británie + kolónií + ďalšie krajny ako Dánsko, Francúzsko a Švajčiarsko
 - celkom 13 000 exponátov

## Michael Thonet (1796 - 1871)

![Stolička č. 214](/images/thonet-chair-214.jpg)
![Stolička č. 14](/images/thonet-chair-14.jpg)

 - továrne na nábytok z ohýbaného dreva - TON
 - uznávaný výrobca nábytku

## Reformné hnutie 2. polovičky 19. storočia
 - Arts and Crafts - od 60. rokov - preraffaelisti + neogotický revival (Augustus W. N. Pugin); priamý ideový otec John Ruskin

## William Morris (1834 - 1896)

![Stoličky](/images/morris-chairs.jpg)
![Tulipány a Ruža](/images/tulips_and_rose.jpg)

 - blízko spätý s Preraffaelistami; predstaviteľ hnutia Arts and Crafts - jeho tvorba považovaná za prvé výsledky
 - zdokonalenie polygrafie

## Chrisopher Dresser (1834 - 1904)

![Sada](/images/dresser-pots-2.jpg)
![Sada](/images/dresser-pots.jpg)
![Držiak na toasty](/images/dresser-toast-rack.jpg)
![Tanier s perským vzorom](/images/dresser-soup-plate-persia.jpg)

 - považovaný za prvého a najdôležitejšieho nezávislého dizajnéra svojej doby
 - Aesthetic Movement + Anglo-Japanese/Modern štýl
 - jednoduché, čisté línie sa niekedy dávajú do súvislosti s jeho návštevou Japonska v r. 1876

## Charles Robert Ashbee (1863 - 1942)

![Karafa](/images/ashbee-karafa.jpg)
![Čajník?](/images/ashbee-2.jpg)

 - najvýraznejšia osobnosť druhej generácie hnutia Arts & Crafts
 - založil Guild of Handicraft (Cech umeleckého remesla)
 - reputácia najmä vďaka strieborným riadom, šperkom a nábytkom 

---

## Secessia (~1890)
 - v 90. rokoch v Európe a USA posledný univerználny sloh - secessia
 - zavrhla akademizmus a kopírovanie historických slohov
 - neakceptovala novú realitu priemyselnej civilizácie v plnej šírke
 - spojenie umenia so životom

## Henri van de Velde

![Písascí stôl](/images/velde-desk.jpg)

## Louise Majorelle

![Postel](/images/majorelle-bed.jpg)

## Henri Guimard

![Parížske metro](/images/guimard-metro.jpg)

## Louis Comfort Tiffany (1848 - 1933)
 - zpočiatku maliar - stal sa sklárom

![Lampshade](/images/tiffany-lampshade.jpg)
![Venetian](/images/tiffany-venetian.jpg)
![Tree lamp](/images/tiffany-tree-lamp.jpg)

## Victor, Baron Horta (1861 - 1947)
 - schodisko Hotel Tassel
 - vstup do parížskeho metra

![Stairway](/images/horta-stairway.jpg)
![Museum](/images/horta-museum.jpg)

## Antonio Gaudí (1852 - 1926)

- casa Batló

![Casa Batló](/images/gaudi-casa-batlo.jpg)

## Josef Maria Olbrich (1867 - 1908)

![Vienna](/images/olbrich-secession.jpg)
![Arm chair](/images/olbrich-armchair.jpg)

 - pomáhal Ottovy Wagnerovi s viedenskými budovami metra

## Otto Wagner (1841 - 1918)

 - predstaviteľ secesie
 - najuznávajnejší viedenský architekt

![Karlsplatz](/images/wagner-karlsplatz.jpg)
![Most stadtbagn](/images/wagner-stadtbagn.jpg)

## Alfons Mucha (1860 - 1939)

![Princezna](/images/mucha-princezna.jpg)
![Monte Carlo](/images/mucha-monte-carlo.jpg)

 - najznámejšia maliarska osobnosť európskej secesie
 - napr. slovanská epopej

## Gustav Klucis

![Propagandistický kiosk](/images/klucis-kiosk.jpeg)

## Gerrit Rietveld
 - De Stijl

![Berlínska stolička](/images/rietveld-berlin-chair.jpg)
![Červeno-Modrá stolička](/images/rietveld-red-blue-chair.jpg)
![Táčky](/images/rietveld-wheelbarrow.jpg)

## Zárodky moderny
- nahradzovanie secesných kriviek priamkami
- abstraktné geometrické obrazce
- zdroj Glasgov - Mackintosh

## Charles Rennie Mackintosh (1868 - 1928)

![Glasgow style](/images/mackintosh-glasgow-style.jpg)
![Teardrop Roses](/images/mackintosh-teardrop-roses.jpg)

 - otec glasgovského štýlu

## Jozef Hoffmann (1870 - 1956)

![Set of Four](/images/josef_hoffmann_set_of_four.jpg)
![Jugendstil, polohovateľné kreslo, Jacob & Josef Kohn](/images/josef_hoffmann_seat_machine.jpg)

 - architekt, dizajnér

## Corporate identity (~1950)
 - 50\. roky 20. storočia
 - rozmach v 70 a 80. rokoch v USA - manuál pomohol vytvoriť celú radu dodnes fungujúcich značiek

## Peter Behrens (1868 - 1940)
 - maliar, ilustrátor, kníhviazač - neskôr architekt a dizajnér
 - identita spoločnosti AEG - logotyp, dizajn produktov, vzťahy s verejnosťou a spôsob prezentácie 
 - **prvý industriálny dizajnér na svete**
 - "zakladateľom" brandingu a prvým brandmanagerom
 - neologická synagoga v Žiline
 - spoluvytváral charakter mníchovskej secesie, neskôr dospel k názoru, že secesné formy sťažuju realizáciu tých tvorivých princípov, ktoré zodpovedajú novému spôsobu života

![Elektrické čajníky AEG](/images/behrens-kettle-aeg.jpg)
![Lampa pre AEG](/images/behrens-lamp-aeg.jpg)
![Ohrievač](/images/behrens-radiant-heater.jpg)

## Fonty

 - Grotesk
 - Bodoni